# Example Swagger UI API documentation site using GitLab Pages.

See this at https://mdsrepl.gitlab.io/gitlab-pages-swagger-ui/

## What is this

I forked this from the kind mdhtr

Learn more about GitLab Pages at https://pages.gitlab.io and the official documentation https://docs.gitlab.com/ce/user/project/pages/

Learn more about Swagger UI at https://github.com/swagger-api/swagger-ui/

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: node:10-alpine

variables:
  DOCS_FOLDER: "api-docs"

cache:
  paths:
  - ./node_modules

pages:
  stage: deploy
  script:
  - npm install swagger-ui-dist@3.22.1
  - mkdir public
  - cp -rp node_modules/swagger-ui-dist/* public
  - rm public/package.json
  - rm public/index.js
  - rm public/absolute-path.js
  - rm public/README.md
  - cp -rp $DOCS_FOLDER/* public
  - sed -i "s#https://petstore\.swagger\.io/v2/swagger\.json#$(ls -t $DOCS_FOLDER | head -n1)#g" public/index.html
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your OpenAPI documentation files in the `api-docs/` directory.
The page will display the newest file by default, but all files form the `api-docs/` folder will be available to be viewed on the page given the correct filename.

[ci]: https://about.gitlab.com/gitlab-ci/

## Gitlab Pages

The resulting page will be available on the standard GitLab Pages subdomain.

For the results of this example 